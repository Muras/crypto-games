// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'


var accounts;
var account;
//var Web3 = require('web3');
// create an instance of web3 using the HTTP provider.
// NOTE in mist web3 is already available, so check first if it's available before instantiating
//var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

var abi = [
	{
		"constant": true,
		"inputs": [],
		"name": "promoCreatedCount",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "name",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "pure",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_to",
				"type": "address"
			},
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "approve",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "ceoAddress",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_to",
				"type": "address"
			}
		],
		"name": "payout",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "implementsERC721",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "pure",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalSupply",
		"outputs": [
			{
				"name": "total",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "getGameDetails",
		"outputs": [
			{
				"name": "gameName",
				"type": "string"
			},
			{
				"name": "sellingPrice",
				"type": "uint256"
			},
			{
				"name": "owner",
				"type": "address"
			},
			{
				"name": "token_id",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_from",
				"type": "address"
			},
			{
				"name": "_to",
				"type": "address"
			},
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "transferFrom",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_newCEO",
				"type": "address"
			}
		],
		"name": "setCEO",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_newCOO",
				"type": "address"
			}
		],
		"name": "setCOO",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "ownerOf",
		"outputs": [
			{
				"name": "owner",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_owner",
				"type": "address"
			}
		],
		"name": "balanceOf",
		"outputs": [
			{
				"name": "balance",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_owner",
				"type": "address"
			}
		],
		"name": "tokensOfOwner",
		"outputs": [
			{
				"name": "ownerTokens",
				"type": "uint256[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "personIndexToApproved",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "symbol",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "pure",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "NAME",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_to",
				"type": "address"
			},
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "transfer",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "cooAddress",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "takeOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "priceOf",
		"outputs": [
			{
				"name": "price",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_name",
				"type": "string"
			}
		],
		"name": "createContractGame",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_owner",
				"type": "address"
			},
			{
				"name": "_name",
				"type": "string"
			},
			{
				"name": "_price",
				"type": "uint256"
			}
		],
		"name": "createPromoGame",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "gameIndexToOwner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_tokenId",
				"type": "uint256"
			}
		],
		"name": "purchase",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "SYMBOL",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "tokenId",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "name",
				"type": "string"
			},
			{
				"indexed": false,
				"name": "owner",
				"type": "address"
			}
		],
		"name": "Birth",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "tokenId",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "oldPrice",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "newPrice",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "prevOwner",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "winner",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "name",
				"type": "string"
			}
		],
		"name": "TokenSold",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "from",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "tokenId",
				"type": "uint256"
			}
		],
		"name": "Transfer",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "approved",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "tokenId",
				"type": "uint256"
			}
		],
		"name": "Approval",
		"type": "event"
	}
];

var bytecode = '606060405266038d7ea4c6800060005566be78bd4c57d0006001556707d72165f25ed000600255341561003157600080fd5b33600760006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555033600860006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506120af806100c26000396000f300606060405260043610610154576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806305e455461461015957806306fdde0314610182578063095ea7b3146102105780630a0f8168146102525780630b7e9c44146102a75780631051db34146102e057806318160ddd1461030d5780631b31abda1461033657806323b872dd1461041357806327d7874c146104745780632ba73c15146104ad5780636352211e146104e657806370a08231146105495780638462151c146105965780639433a81e1461062457806395d89b4114610687578063a3f4df7e14610715578063a9059cbb146107a3578063b047fb50146107e5578063b2e6ceeb1461083a578063b9186d7d1461085d578063ce6f6e8814610894578063cfc8b7f5146108f1578063ed2f79f814610976578063efef39a1146109d9578063f76f8d78146109f1575b600080fd5b341561016457600080fd5b61016c610a7f565b6040518082815260200191505060405180910390f35b341561018d57600080fd5b610195610a85565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156101d55780820151818401526020810190506101ba565b50505050905090810190601f1680156102025780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561021b57600080fd5b610250600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091908035906020019091905050610ac8565b005b341561025d57600080fd5b610265610b98565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34156102b257600080fd5b6102de600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610bbe565b005b34156102eb57600080fd5b6102f3610c7e565b604051808215151515815260200191505060405180910390f35b341561031857600080fd5b610320610c87565b6040518082815260200191505060405180910390f35b341561034157600080fd5b6103576004808035906020019091905050610c94565b60405180806020018581526020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001838152602001828103825286818151815260200191508051906020019080838360005b838110156103d55780820151818401526020810190506103ba565b50505050905090810190601f1680156104025780820380516001836020036101000a031916815260200191505b509550505050505060405180910390f35b341561041e57600080fd5b610472600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803573ffffffffffffffffffffffffffffffffffffffff16906020019091908035906020019091905050610db4565b005b341561047f57600080fd5b6104ab600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610e02565b005b34156104b857600080fd5b6104e4600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610ede565b005b34156104f157600080fd5b6105076004808035906020019091905050610fba565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561055457600080fd5b610580600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050611033565b6040518082815260200191505060405180910390f35b34156105a157600080fd5b6105cd600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190505061107c565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b838110156106105780820151818401526020810190506105f5565b505050509050019250505060405180910390f35b341561062f57600080fd5b61064560048080359060200190919050506111b3565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561069257600080fd5b61069a6111e6565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156106da5780820151818401526020810190506106bf565b50505050905090810190601f1680156107075780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561072057600080fd5b610728611229565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561076857808201518184015260208101905061074d565b50505050905090810190601f1680156107955780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156107ae57600080fd5b6107e3600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091908035906020019091905050611262565b005b34156107f057600080fd5b6107f861129a565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b341561084557600080fd5b61085b60048080359060200190919050506112c0565b005b341561086857600080fd5b61087e6004808035906020019091905050611335565b6040518082815260200191505060405180910390f35b341561089f57600080fd5b6108ef600480803590602001908201803590602001908080601f01602080910402602001604051908101604052809392919081815260200183838082843782019150505050505091905050611352565b005b34156108fc57600080fd5b610974600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803590602001908201803590602001908080601f016020809104026020016040519081016040528093929190818152602001838380828437820191505050505050919080359060200190919050506113be565b005b341561098157600080fd5b61099760048080359060200190919050506114bf565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b6109ef60048080359060200190919050506114f2565b005b34156109fc57600080fd5b610a0461189e565b6040518080602001828103825283818151815260200191508051906020019080838360005b83811015610a44578082015181840152602081019050610a29565b50505050905090810190601f168015610a715780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b60095481565b610a8d611ee5565b6040805190810160405280600b81526020017f43727970746f47616d6573000000000000000000000000000000000000000000815250905090565b610ad233826118d7565b1515610add57600080fd5b816005600083815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508173ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925836040518082815260200191505060405180910390a35050565b600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161480610c675750600860009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16145b1515610c7257600080fd5b610c7b81611943565b50565b60006001905090565b6000600a80549050905090565b610c9c611ee5565b600080600080600a86815481101515610cb157fe5b90600052602060002090019050806000018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610d565780601f10610d2b57610100808354040283529160200191610d56565b820191906000526020600020905b815481529060010190602001808311610d3957829003601f168201915b50505050509450600660008781526020019081526020016000205493506003600087815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169250859150509193509193565b610dbe83826118d7565b1515610dc957600080fd5b610dd38282611a51565b1515610dde57600080fd5b610de782611abd565b1515610df257600080fd5b610dfd838383611af6565b505050565b600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610e5e57600080fd5b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff1614151515610e9a57600080fd5b80600760006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610f3a57600080fd5b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff1614151515610f7657600080fd5b80600860006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b60006003600083815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff161415151561102e57600080fd5b919050565b6000600460008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020549050919050565b611084611ef9565b600061108e611ef9565b600080600061109c87611033565b945060008514156110ce5760006040518059106110b65750595b908082528060200260200182016040525095506111a9565b846040518059106110dc5750595b908082528060200260200182016040525093506110f7610c87565b925060009150600090505b82811115156111a5578673ffffffffffffffffffffffffffffffffffffffff166003600083815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1614156111985780848381518110151561118157fe5b906020019060200201818152505081806001019250505b8080600101915050611102565b8395505b5050505050919050565b60056020528060005260406000206000915054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6111ee611ee5565b6040805190810160405280600981526020017f47616d65546f6b656e0000000000000000000000000000000000000000000000815250905090565b6040805190810160405280600b81526020017f43727970746f47616d657300000000000000000000000000000000000000000081525081565b61126c33826118d7565b151561127757600080fd5b61128082611abd565b151561128b57600080fd5b611296338383611af6565b5050565b600860009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b6000803391506003600084815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905061130582611abd565b151561131057600080fd5b61131a8284611a51565b151561132557600080fd5b611330818385611af6565b505050565b600060066000838152602001908152602001600020549050919050565b600860009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156113ae57600080fd5b6113bb8130600054611cf8565b50565b6000600860009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561141c57600080fd5b61138860095410151561142e57600080fd5b839050600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff16141561148c57600860009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690505b60008211151561149c5760005491505b6009600081548092919060010191905055506114b9838284611cf8565b50505050565b60036020528060005260406000206000915054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b60008060008060006003600087815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169450339350600660008781526020019081526020016000205492508373ffffffffffffffffffffffffffffffffffffffff168573ffffffffffffffffffffffffffffffffffffffff161415151561158457600080fd5b61158d84611abd565b151561159857600080fd5b8234101515156115a757600080fd5b6115bc6115b584605f611e76565b6064611eb1565b91506115c83484611ecc565b9050600154831015611605576115e96115e28460c8611e76565b605e611eb1565b600660008881526020019081526020016000208190555061166e565b6002548310156116405761162461161d846078611e76565b605e611eb1565b600660008881526020019081526020016000208190555061166d565b61165561164e846073611e76565b605e611eb1565b60066000888152602001908152602001600020819055505b5b611679858588611af6565b3073ffffffffffffffffffffffffffffffffffffffff168573ffffffffffffffffffffffffffffffffffffffff161415156116ef578473ffffffffffffffffffffffffffffffffffffffff166108fc839081150290604051600060405180830381858888f1935050505015156116ee57600080fd5b5b7e8201e7bcbf010c2c07de59d6e97cb7e3cf67a46125c49cbc89b9d2cde1f48f8684600660008a8152602001908152602001600020548888600a8c81548110151561173657fe5b9060005260206000209001600001604051808781526020018681526020018581526020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001806020018281038252838181546001816001161561010002031660029004815260200191508054600181600116156101000203166002900480156118435780601f1061181857610100808354040283529160200191611843565b820191906000526020600020905b81548152906001019060200180831161182657829003601f168201915b505097505050505050505060405180910390a13373ffffffffffffffffffffffffffffffffffffffff166108fc829081150290604051600060405180830381858888f19350505050151561189657600080fd5b505050505050565b6040805190810160405280600981526020017f47616d65546f6b656e000000000000000000000000000000000000000000000081525081565b60006003600083815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168373ffffffffffffffffffffffffffffffffffffffff1614905092915050565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff1614156119f657600760009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc3073ffffffffffffffffffffffffffffffffffffffff16319081150290604051600060405180830381858888f1935050505015156119f157600080fd5b611a4e565b8073ffffffffffffffffffffffffffffffffffffffff166108fc3073ffffffffffffffffffffffffffffffffffffffff16319081150290604051600060405180830381858888f193505050501515611a4d57600080fd5b5b50565b60008273ffffffffffffffffffffffffffffffffffffffff166005600084815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1614905092915050565b60008073ffffffffffffffffffffffffffffffffffffffff168273ffffffffffffffffffffffffffffffffffffffff1614159050919050565b600460008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008154809291906001019190505550816003600083815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550600073ffffffffffffffffffffffffffffffffffffffff168373ffffffffffffffffffffffffffffffffffffffff16141515611c5457600460008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008154809291906001900391905055506005600082815260200190815260200160002060006101000a81549073ffffffffffffffffffffffffffffffffffffffff02191690555b7fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef838383604051808473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001828152602001935050505060405180910390a1505050565b611d00611f0d565b60006020604051908101604052808681525091506001600a8054806001018281611d2a9190611f27565b916000526020600020900160008590919091506000820151816000019080519060200190611d59929190611f53565b5050500390508063ffffffff1681141515611d7357600080fd5b7fb3b0cf861f168bcdb275c69da97b2543631552ba562628aa3c7317d4a6089ef281868660405180848152602001806020018373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001828103825284818151815260200191508051906020019080838360005b83811015611e0f578082015181840152602081019050611df4565b50505050905090810190601f168015611e3c5780820380516001836020036101000a031916815260200191505b5094505050505060405180910390a1826006600083815260200190815260200160002081905550611e6f60008583611af6565b5050505050565b6000806000841415611e8b5760009150611eaa565b8284029050828482811515611e9c57fe5b04141515611ea657fe5b8091505b5092915050565b6000808284811515611ebf57fe5b0490508091505092915050565b6000828211151515611eda57fe5b818303905092915050565b602060405190810160405280600081525090565b602060405190810160405280600081525090565b602060405190810160405280611f21611fd3565b81525090565b815481835581811511611f4e57818360005260206000209182019101611f4d9190611fe7565b5b505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10611f9457805160ff1916838001178555611fc2565b82800160010185558215611fc2579182015b82811115611fc1578251825591602001919060010190611fa6565b5b509050611fcf9190612016565b5090565b602060405190810160405280600081525090565b61201391905b8082111561200f5760008082016000612006919061203b565b50600101611fed565b5090565b90565b61203891905b8082111561203457600081600090555060010161201c565b5090565b90565b50805460018160011615610100020316600290046000825580601f106120615750612080565b601f01602090049060005260206000209081019061207f9190612016565b5b505600a165627a7a723058201fef839a0b3c1a9c05cd5e1f66462091535e81f07ba94cf2f7523fd908fd4b9d0029';
var userAbi = [
	{
		"constant": true,
		"inputs": [
			{
				"name": "_username",
				"type": "string"
			},
			{
				"name": "_password",
				"type": "string"
			}
		],
		"name": "loginUser",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "users",
		"outputs": [
			{
				"name": "username",
				"type": "string"
			},
			{
				"name": "password",
				"type": "string"
			},
			{
				"name": "fullname",
				"type": "string"
			},
			{
				"name": "account",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "kill",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_username",
				"type": "string"
			}
		],
		"name": "validateUsername",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "userAccount",
				"type": "address"
			}
		],
		"name": "getReviewers",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			},
			{
				"name": "",
				"type": "address"
			},
			{
				"name": "",
				"type": "address"
			},
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_username",
				"type": "string"
			},
			{
				"name": "_password",
				"type": "string"
			},
			{
				"name": "_fullname",
				"type": "string"
			},
			{
				"name": "_account",
				"type": "address"
			}
		],
		"name": "addUser",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalUsers",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "constructor"
	}
];

var contractAddress = "0xc959b1c08d2f91a5608c8aab75e0f0a74d90a48e";

var gamingContract=web3.eth.contract(abi).at(contractAddress);
var userAddress = "0x5d40fca7ba9eb4b84f1fa9e5befc59a1faf8f9a7";
var userContract=web3.eth.contract(userAbi).at(userAddress);


var myList = new Array();
var count = 0;

window.App = {
  start: function() {
    var self = this;

    // Bootstrap the MetaCoin abstraction for Use.
      // Get the initial account balance so it can be displayed.
   web3.eth.defaultAccount = web3.eth.accounts[0];
   web3.eth.getAccounts(function(err, accs) {

      if (err != null) {
        alert("online");
        accounts = accs;
        account = accounts[0];

        return;
      }

      if (accs.length == 0) {
        alert("offline");
				alert("Please Login Metamask");
        return;
      }
      accounts = accs;
      account = accounts[0];

			// document.getElementById('etherAddress').innerHTML= account;
			gamingContract.cooAddress.call(function(error,results){
				if(error){
					console.log(error);
				}

				if(account == results){
					document.getElementById('addnewgame').style.display = "block";
				}
				else {
					document.getElementById('addnewgame').style.display = "none";
				}
			})

			self.totalGames();

    });
  },
totalGames:function()
  {
		var self = this;
		 myList = [];
     gamingContract.totalSupply.call(function(error,results){
     if(error != null)
        alert("error");
      else{

				var total= 0;
				userContract.totalUsers.call(function(err,totaluser){
					if(err)
					console.log("userDetails"+error)
					else{
						//alert("total user"+totaluser);
				document.getElementById('featuredProducts').innerHTML = "";
				var featuredProductsdiv = document.getElementById("featuredProducts");

				for(total = 0;total<results;total++){
          gamingContract.getGameDetails.call(total,function(error,result)
          {
            if(error != null)
              alert("You Have Error");
            else {

							var game = {"name":result[0],"owner":result[1],"cost":result[2],"id":result[3]};
							myList.push(game);
							var value = web3.fromWei(result[1], 'ether');

							var gamesdiv = document.createElement("div");
							gamesdiv.classList.add("col-md-4");
							var productsdiv = document.createElement("div");
							productsdiv.classList.add("card");
							var uldiv = document.createElement("ul");
							var lidiv = document.createElement("li");
							var buy =document.createElement("div");
							buy.onclick = function(){
								window.location.href = "tokens.html?tokenId="+result[3]+"&name="+result[0]+"&cost="+value;
							}
							var h3 = document.createElement("h3");
							h3.innerHTML = result[0];
							var h5 = document.createElement("h5");
							h5.innerHTML = value+" Eth" ;
							var h6 = document.createElement("h6");
							h6.innerHTML = "Owner ";
							var p = document.createElement("p");
							// p.innerHTML = result[2];


							for(var user=0;user<totaluser;user++){

							userContract.users.call(user,function(error,userdetail){

								//alert(userdetail)
								if(error)
								console.log(error);
								//alert(userdetail);
								else {
									//alert(userdetail)
									if(userdetail[3] == result[2])
										p.innerHTML = userdetail[2];


								}
							});
							}
							if(p.innerHTML == ""){
							p.innerHTML = result[2];
							}
							var img = document.createElement("img");
							img.src = "app/images/"+result[0]+".png";
							var button = document.createElement("button");
							button.innerHTML = "Buy it";

							button.classList.add("btn");
							button.classList.add("btn-outline-success");
							buy.appendChild(img);
							buy.appendChild(h3);

							buy.appendChild(h5);
							lidiv.appendChild(h6);
							lidiv.appendChild(p);
							lidiv.appendChild(button);

							button.addEventListener ("click", function() {

											self.tokenPurchase(result[3],value);
							});

							uldiv.appendChild(buy);
							uldiv.appendChild(lidiv);
							productsdiv.appendChild(uldiv);
							gamesdiv.appendChild(productsdiv);
							featuredProductsdiv.appendChild(gamesdiv);

            	}
        	});
				}
			}
				})
			}
		});
},

yourTokens:function(address){

	var self = this;
	//alert(address)
	gamingContract.tokensOfOwner.call(address,function(error,result)
			{
				if(error != null)
							 alert("You Have Error");
				if(result.length == 0){
								alert("You have No Tokens");
								self.totalGames();
							}
				else{
								 document.getElementById('featuredProducts').innerHTML = "";
				 				 var featuredProductsdiv = document.getElementById("featuredProducts");

							   for(var total = 0;total<result.length;total++){
								 gamingContract.getGameDetails.call(result[total],function(error,result)
								 {
									 if(error != null)
									 {
										 alert("You Have Error");
									 }
									 else {

										var value = web3.fromWei(result[1], 'ether');

										var gamesdiv = document.createElement("div");

										gamesdiv.classList.add("col-md-4");
										var productsdiv = document.createElement("div");
										productsdiv.classList.add("card");


										var uldiv = document.createElement("ul");
										var lidiv = document.createElement("li");

										var h3 = document.createElement("h3");
										h3.innerHTML = result[0];

										var h5 = document.createElement("h5");
										h5.innerHTML = value+" Eth" ;

										var img = document.createElement("img");
										img.src = "app/images/"+result[0]+".png";
										var button = document.createElement("button");
										button.innerHTML = "Yours";

										button.classList.add("btn");
										button.classList.add("btn-outline-success");


										lidiv.appendChild(img);
										lidiv.appendChild(h3);
										lidiv.appendChild(h5);
										//lidiv.appendChild(h6);
										lidiv.appendChild(button);

										uldiv.appendChild(lidiv);
										productsdiv.appendChild(uldiv);
										gamesdiv.appendChild(productsdiv);
										featuredProductsdiv.appendChild(gamesdiv);
									}

								});
							 };
						 }

					 });

},
totalTransactions :function(){


	var self = this;
	self.clearTable();
		var currentIndex = 0;
		document.getElementById('featuredProducts').innerHTML = "";

		userContract.totalUsers.call(function(error,totaluser){
			if(error)
			console.log(error);
			else{

		var myEvent = gamingContract.Transfer({},{fromBlock: 0, toBlock: 'latest'});
		myEvent.watch(function(error, result){
	//alert("Token From : "+result.args.from+" Token to : "+ result.args.to+"Tooken Id : "+result.args.tokenId)


	var ulDiv = document.getElementById("ul");
var ul = document.createElement("ul");

	var dateLi =document.createElement("li");
	var from =document.createElement("li");
	var to =document.createElement("li");
	var gameName =document.createElement("li");
	var balanceHistory =document.createElement("li");
	web3.eth.getBlock(result.blockNumber, function(error, result){
	 if(!error){
		 var time = new Date(result.timestamp * 1000);
		 dateLi.innerHTML = time.toLocaleString();}
	 else
		 console.log(error);});
		 if(result.args.from == "0x0000000000000000000000000000000000000000")
			from.innerHTML = "Game Created";
			else

			for(var user=0;user<totaluser;user++){
				userContract.users.call(user,function(error,userdetails){

				if(error)
				console.log(error);
				 if(userdetails[3] == result.args.from){
						from.innerHTML = userdetails[2];
						//cell3.innerHTML = result.args.to;
		}

		});
		}
		if(from.innerHTML == "")
		{
			from.innerHTML = result.args.from;
		}
		for(var user=0;user<totaluser;user++){
			userContract.users.call(user,function(error,userdetails){

			if(error)
			console.log(error);
			 if(userdetails[3] == result.args.to){
					to.innerHTML = "To " +userdetails[2];
					//cell3.innerHTML = result.args.to;
	}

	});
	}
		if(to.innerHTML == "")
		{
			to.innerHTML = "To " +result.args.to;
		}
		var name =
 		gamingContract.getGameDetails.call(result.args.tokenId,function(error,result){
 		if(error != null)
 		alert("Error");
 		else
 		gameName.innerHTML = result[0];})

		web3.eth.getTransaction(result.transactionHash,function(error,result){
		if(error)
			console.log(error);
		else{

			var balance =  web3.fromWei(result.value, 'ether');
			if(balance == 0)
				balanceHistory.innerHTML = "Price 0.0001EtH";
			else
				balanceHistory.innerHTML = "Price"+balance+"ETH";
		}
	})

		ul.appendChild(dateLi);
		ul.appendChild(from);
		ul.appendChild(to);
		ul.appendChild(gameName);
		ul.appendChild(balanceHistory);
		ulDiv.appendChild(ul);



	});
}
});
},

clearTable:function(){
   // var ul = document.getElementById("transactions");
   // var rowCount = table.rows.length;
   // for (var i = 1; i < rowCount; i++) {
   //    table.deleteRow(1);
   // }
	 document.getElementById('ul').innerHTML = "";
},

getGameDetail : function(tokenId){

	gamingContract.getGameDetails.call(tokenId,function(error,result){
		if(error != null)
			alert("Error");
		else
		 	return result;
	})
},

createNewGame : function(){

	var gameName = document.getElementById('gameName').value;
	if(gameName == ''){
		alert("Enter Game name");
	}
	else{
	gamingContract.createContractGame(gameName,{from:account,gas:3000000},function(error,result)
		 {
			 if(error != null)
			 	alert("Error");
			else {
				alert("Image Name Should Be game Name");
				alert("Game will be added soon");
			}
		});
	}
},

tokenPurchase :function(token_id,cost){

	// alert("Cost for Game Token is : "+cost+"eth");
	var token_id = token_id;
	var cost = cost;
	gamingContract.purchase(token_id,{from:account,value : web3.toWei(cost, "ether")},function(error,result)
		 {
			 if(error != null)
				alert("Error");
			else {
						//alert("Your Hash is : "+result);
						//alert("Token will be added Your wallet soon");
			}
		});
},

getParameterByName : function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
},

printValue : function(){

		var self = this;
		//document.getElementById('featuredProduct').innerHTML = "";
		//var featuredProductsdiv = document.getElementById("featuredProduct");

		var tokenId = App.getParameterByName("tokenId");
		var nameValue = App.getParameterByName("name");
		var costValue = App.getParameterByName("cost");
		var img = document.createElement("img");
		document.getElementById("tokenId").innerHTML = tokenId;
		document.getElementById("name").innerHTML = nameValue;
		document.getElementById("cost").innerHTML = costValue;
		img.src = "app/images/"+nameValue+".png";
		document.getElementById("img").appendChild(img);

		self.tokenTransaction(tokenId);
},
tokenTransaction :function(tokenId){

	var self = this;
	self.clearTable();

	var currentIndex = 0;
	var nextprice ='';
userContract.totalUsers.call(function(error,totaluser){
if(error)
console.log(error);
else{
	//alert(totaluser);
	var myEvent = gamingContract.TokenSold({},{fromBlock: 0, toBlock: 'latest'});
	myEvent.watch(function(error, result){
		if(error)
		console.log(error);
		else {
			if(result.args.tokenId == tokenId){
				var ulDiv = document.getElementById("ul");
			var ul = document.createElement("ul");

				var dateLi =document.createElement("li");
				var from =document.createElement("li");
				var to =document.createElement("li");
				var gameName =document.createElement("li");
				var balanceHistory =document.createElement("li");
				web3.eth.getBlock(result.blockNumber, function(error, result){
				 if(!error){
					 var time = new Date(result.timestamp * 1000);
					 dateLi.innerHTML = time.toLocaleString();}
				 else
					 console.log(error);});
					 if(result.args.prevOwner == "0x0000000000000000000000000000000000000000")
						from.innerHTML = "Game Created";
						else

						for(var user=0;user<totaluser;user++){
							userContract.users.call(user,function(error,userdetails){

							if(error)
							console.log(error);
							 if(userdetails[3] == result.args.prevOwner){
									from.innerHTML = userdetails[2];
									//cell3.innerHTML = result.args.to;
					}

					});
					}
					if(from.innerHTML == "")
					{
						from.innerHTML = result.args.prevOwner;
					}
					for(var user=0;user<totaluser;user++){
						userContract.users.call(user,function(error,userdetails){

						if(error)
						console.log(error);
						 if(userdetails[3] == result.args.winner){
								to.innerHTML = userdetails[2];
								//cell3.innerHTML = result.args.to;
				}

				});
				}
					if(to.innerHTML == "")
					{
						to.innerHTML = result.args.winner;
					}
					var name =
			 		gamingContract.getGameDetails.call(result.args.tokenId,function(error,result){
			 		if(error != null)
			 		alert("Error");
			 		else
			 		gameName.innerHTML = result[0];})

					web3.eth.getTransaction(result.transactionHash,function(error,result){
					if(error)
						console.log(error);
					else{

						var balance =  web3.fromWei(result.value, 'ether');
						if(balance == 0)
							balanceHistory.innerHTML = "0.0001Eth";
						else
							balanceHistory.innerHTML = balance+"Eth";
					}
				})

					ul.appendChild(dateLi);
					//ul.appendChild(from);
					ul.appendChild(to);
					//ul.appendChild(gameName);
					ul.appendChild(balanceHistory);
					ulDiv.appendChild(ul);


			//alert(result.args.tokenId)
		}
			else {
				//alert("This is a new Token");
				console.log("No tokens");
			}
		}
	});
}
})

},
registerNewUser:function(fullname,username,password){

	var self  = this;
	//alert("here"+username+" "+fullname+" "+password);
	userContract.validateUsername.call(username,function(error,result){
		if(error){
		console.log(error);
	}


	if(result == false){
		//alert("Username exists");
		document.getElementById('errorstatus').innerHTML = "username exists";
		return;
	 }

	 if(result == true){
	 		//alert("here");
	 		self.addUser(fullname,username,password);
	// 	}
}
	});

},

addUser:function(fullname,username,password){
	userContract.addUser(username,password,fullname,account,{from : account},function(error,result){
		if(error){
		console.log(error);
	}
	else{
		window.location.href = "login.html"
		document.getElementById('errorstatus').innerHTML = "User will be added When transaction completed"
	}
});
},
loginuser:function(username,password){
	userContract.loginUser.call(username,password,function(error,result){
		if(error){
		console.log(error);
	}
	//alert("here"+result)
	if(result[0] == true){
		window.location.href = "users.html?address="+result[2]+"&name="+result[1];
	}
	else{
		document.getElementById('errorstatus').innerHTML = "username password incorrect"
		return;
		//document.getElementById('status').innerHTML = "User will be added When transaction completed"
	}
	});
},
userDetails:function(){
	var name = App.getParameterByName("name");
	var Address = App.getParameterByName("address");
	document.getElementById('name').innerHTML = name;
	document.getElementById('address').innerHTML = Address;
}
}

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://127.0.0.1:9545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:9545"));
  }

  App.start();
});
